package rg.webfluxswagger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.reactive.config.EnableWebFlux;

@EnableWebFlux
@SpringBootApplication
public class WebfluxSwaggerApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebfluxSwaggerApplication.class, args);
	}

}

