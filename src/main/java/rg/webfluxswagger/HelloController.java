package rg.webfluxswagger;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

import java.time.Duration;
import java.util.Iterator;
import java.util.UUID;

@RestController
public class HelloController {

    private static final Iterator<String> RANDOM_ITERATOR = new Iterator<>() {
        @Override
        public boolean hasNext() {
            return true;
        }

        @Override
        public String next() {
            return UUID.randomUUID().toString();
        }
    };

    @GetMapping(path = "/list")
    public Flux<String> list() {
        return randomIntervalFlux().take(3);
    }

    @GetMapping(path = "/stream", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<String> streamInfinit() {
        return randomIntervalFlux();
    }

    @GetMapping(path = "/stream/{elements}", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<String> stream(@PathVariable int elements) {
        return randomIntervalFlux().take(elements);
    }

    private Flux<String> randomIntervalFlux() {
        return Flux.zip(Flux.fromIterable(() -> RANDOM_ITERATOR), Flux.interval(Duration.ofSeconds(1)), (r, i) -> r);
    }
}
